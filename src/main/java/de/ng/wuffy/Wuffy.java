package de.ng.wuffy;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.PropertyConfigurator;

import de.ng.wuffy.command.CommandManager;
import de.ng.wuffy.lang.LanguageManager;
import de.ng.wuffy.listener.MessageListener;
import de.ng.wuffy.listener.ReadyListener;
import de.ng.wuffy.settings.GuildSettingsManager;
import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.api.events.EventDispatcher;

public class Wuffy {

	private static final String BOT_TOKEN = "MzI3MjY3OTUzMTc3NTI2Mjcz.DHDRVg.froafp5KI_YCk6-TSbay9Z0OSc0";
	public static final Long[] BOT_AUTHORS = new Long[] { 128286216708685824L, 98065835859382272L };
	
	public static final String EMBED_AUTHOR_ICON = "http://173.249.11.205/Pictures/Ng_avatar.png";
	public static final String EMBED_AUTHOR_NAME = "Wuffy";
	public static final String EMBED_AUTHOR_URL = "https://zockercraft.net";

	public static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(2);

	public static IDiscordClient client;
	
	public static void main(String[] args) throws Exception {
		System.out.println("Starting wuffy.\nSetup...");

		/* Setup logger */
		System.out.println("Configure log4j...");
		PropertyConfigurator.configure("./log4j.properties");

		/* Setup language */
		System.out.println("Load default language... (en_US)");
		LanguageManager.LANGUAGE_MANAGER.load("en_US");
		
		/* Loading guild settings */
		GuildSettingsManager.GUILD_SETTINGS_MANAGER.loadAllGuilds();
		
		/* Register commands */
		CommandManager.COMMAND_MANAGER.registerCommands();
		
		/* Login to discord */
		System.out.println("Building client...");
		client = new ClientBuilder().withToken(BOT_TOKEN).build();

		/* Adding Listeners */
		System.out.println("Register listener...");
		EventDispatcher dispatcher = client.getDispatcher();
		dispatcher.registerListener(new MessageListener());
		dispatcher.registerListener(new ReadyListener());

		System.out.println("Setup finished. Client is now ready and try to connect :)");
		client.login();
		
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			System.out.println("Good night my friend.");
			
			GuildSettingsManager.GUILD_SETTINGS_MANAGER.saveAllGuilds();
		}));
	}
}