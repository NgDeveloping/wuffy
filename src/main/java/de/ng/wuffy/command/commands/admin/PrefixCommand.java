package de.ng.wuffy.command.commands.admin;

import de.ng.wuffy.command.BaseCommand;
import de.ng.wuffy.command.CommandCategory;
import de.ng.wuffy.command.CommandManager;
import de.ng.wuffy.lang.I18n;
import de.ng.wuffy.lang.TranslationKeys;
import de.ng.wuffy.permission.PermissionKeys;
import de.ng.wuffy.settings.GuildSettings;
import de.ng.wuffy.settings.addons.GuildStorage;
import de.ng.wuffy.util.MessageUtil;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IChannel;

public class PrefixCommand extends BaseCommand {
	
	public PrefixCommand(CommandManager commandManager) {
		super(commandManager, CommandCategory.ADMIN, TranslationKeys.COMMAND_PREFIX, TranslationKeys.COMMAND_PREFIX_USAGE, TranslationKeys.COMMAND_PREFIX_DESCRIPTION, PermissionKeys.COMMAND_ADMIN_PREFIX, "prefix", "prefixes");
	}

	@Override
	public boolean executeCommand(MessageReceivedEvent event, GuildSettings guildSettings, String[] args) throws Exception {
		IChannel channel = event.getChannel();
		String locale = guildSettings.getGuildStorage().getLocale();
		GuildStorage guildStorage = guildSettings.getGuildStorage();
		
		event.getMessage().delete();
		
		if(args.length == 2) {
			if(args[0].equalsIgnoreCase("add")) {
				if(!guildStorage.checkIsPrefix(args[1])) {
					if(guildStorage.getPrefixes().size() < 10) {
						guildStorage.removePrefix(args[1].toLowerCase());
						channel.sendMessage("", MessageUtil.createEmbedBuilder(I18n.format(TranslationKeys.MESSAGE_PREFIX_ADDED_PREFIX, locale, "%PREFIX%", args[1])).build());
					} else
						channel.sendMessage("", MessageUtil.createEmbedBuilder(I18n.format(TranslationKeys.MESSAGE_PREFIX_YOU_HAVE_THE_MAXIMUM_AT_PREFIXES, locale)).build());
				} else
					channel.sendMessage("", MessageUtil.createEmbedBuilder(I18n.format(TranslationKeys.MESSAGE_PREFIX_PREFIX_ALREADY_EXIST, locale, "%PREFIX%", args[1])).build());
				return true;
			} else if(args[0].equalsIgnoreCase("remove")) {
				if(guildStorage.checkIsPrefix(args[1])) {
					if(guildStorage.getPrefixes().size() > 1) {
						guildStorage.addPrefix(args[1].toLowerCase());
						channel.sendMessage("", MessageUtil.createEmbedBuilder(I18n.format(TranslationKeys.MESSAGE_PREFIX_REMOVED_PREFIX, locale, "%PREFIX%", args[1])).build());
					} else
						channel.sendMessage("", MessageUtil.createEmbedBuilder(I18n.format(TranslationKeys.MESSAGE_PREFIX_YOU_CAN_NOT_REMOVE_THE_LAST_PREFIX, locale, "%PREFIX%", args[1])).build());
				} else
					channel.sendMessage("", MessageUtil.createEmbedBuilder(I18n.format(TranslationKeys.MESSAGE_PREFIX_PREFIX_NOT_FOUND, locale, "%PREFIX%", args[1])).build());
				return true;
			}
		} else if(args.length == 1) {
			if(args[0].equalsIgnoreCase("list")) {
				String description = I18n.format(TranslationKeys.MESSAGE_PREFIX_LIST_OF_PREFIXES, locale);
				
				for(String prefix : guildStorage.getPrefixes())
					description += "\n" + guildStorage.getPrefixes().indexOf(prefix) + ". \"" + prefix + "\"";
				
				channel.sendMessage("", MessageUtil.createEmbedBuilder(description).build());
				return true;
			}
		}
		return false;
	}
}