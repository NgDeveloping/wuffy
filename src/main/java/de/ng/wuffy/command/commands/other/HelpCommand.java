package de.ng.wuffy.command.commands.other;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import de.ng.wuffy.command.BaseCommand;
import de.ng.wuffy.command.CommandCategory;
import de.ng.wuffy.command.CommandManager;
import de.ng.wuffy.lang.I18n;
import de.ng.wuffy.lang.TranslationKeys;
import de.ng.wuffy.permission.PermissionKeys;
import de.ng.wuffy.settings.GuildSettings;
import de.ng.wuffy.util.MessageUtil;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.util.EmbedBuilder;

public class HelpCommand extends BaseCommand {

	public HelpCommand(CommandManager commandManager) {
		super(commandManager, CommandCategory.OTHER, TranslationKeys.COMMAND_HELP, TranslationKeys.COMMAND_HELP_USAGE, TranslationKeys.COMMAND_HELP_DESCRIPTION, PermissionKeys.COMMAND_OTHER_HELP, "help");
	}

	@Override
	public boolean executeCommand(MessageReceivedEvent event, GuildSettings guildSettings, String[] args) throws Exception {
		String locale = guildSettings.getGuildStorage().getLocale();
		
		if(args.length == 0) {
			EmbedBuilder embedBuilder = MessageUtil.createEmbedBuilder(I18n.format(getDescription(), locale));
			
			for(CommandCategory category : CommandCategory.values()) {
				List<TranslationKeys> list = new ArrayList<>();
				
				for(BaseCommand command : commandManager.getCommands().values())
					if(command.getCategory() == category && !list.contains(command.getName()))
						list.add(command.getName());
				if(list.isEmpty())
					list.add(TranslationKeys.MESSAGE_HELP_EMPTY);
				
				embedBuilder.appendField(category.name().substring(0, 1).toUpperCase() + category.name().substring(1).toLowerCase(), String.join("\n", list.stream().map(translationKey -> I18n.format(translationKey, locale)).collect(Collectors.toList())), true);
			}
			event.getChannel().sendMessage(embedBuilder.build());
			event.getMessage().delete();
			return true;
		} else if(args.length == 1) {
			BaseCommand command = commandManager.getCommands().get(args[0].toLowerCase());
			if(command != null) {
				EmbedBuilder embedBuilder = MessageUtil.createEmbedBuilder(I18n.format(TranslationKeys.MESSAGE_HELP_COMMAND_INFO, locale, "%COMMAND%", I18n.format(command.getName(), locale)));
				embedBuilder.appendField(I18n.format(TranslationKeys.MESSAGE_HELP_COMMAND_ALIASES, locale), String.join("\n", command.getAliases()), true);
				event.getChannel().sendMessage("", embedBuilder.build());
			} else
				event.getChannel().sendMessage("", MessageUtil.createEmbedBuilder(I18n.format(TranslationKeys.MESSAGE_HELP_COMMAND_NOT_FOUND, locale)).build());
			return true;
		}
		return false;
	}
}