package de.ng.wuffy.command.commands.admin;

import de.ng.wuffy.command.BaseCommand;
import de.ng.wuffy.command.CommandCategory;
import de.ng.wuffy.command.CommandManager;
import de.ng.wuffy.lang.I18n;
import de.ng.wuffy.lang.TranslationKeys;
import de.ng.wuffy.permission.PermissionKeys;
import de.ng.wuffy.settings.GuildSettings;
import de.ng.wuffy.util.MessageUtil;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.util.EmbedBuilder;

public class PermissionCommand extends BaseCommand {

	public PermissionCommand(CommandManager commandManager) {
		super(commandManager, CommandCategory.ADMIN, TranslationKeys.COMMAND_PERMISSION, TranslationKeys.COMMAND_PERMISSION_USAGE, TranslationKeys.COMMAND_PERMISSION_DESCRIPTION, PermissionKeys.COMMAND_ADMIN_PERMISSION, "permission", "perm", "perms", "permissions");
	}

	@Override
	public boolean executeCommand(MessageReceivedEvent event, GuildSettings guildSettings, String[] args) throws Exception {
		EmbedBuilder embedBuilder = MessageUtil.createEmbedBuilder(I18n.format(TranslationKeys.COMMAND_PERMISSION_DESCRIPTION, guildSettings.getGuildStorage().getLocale()) + "\n\n");
		
		for(CommandCategory category : CommandCategory.values()) {
			String permissionList = "";
			for(PermissionKeys permissionKey : PermissionKeys.values())
				if(permission.getCategory() == category)
					permissionList += permissionKey.getPermission() + "\n";
			if(permissionList.isEmpty())
				permissionList = I18n.format(TranslationKeys.MESSAGE_PERMISSION_EMPTY, guildSettings.getGuildStorage().getLocale());
			
			embedBuilder.appendField(category.name().substring(0, 1).toUpperCase() + category.name().substring(1).toLowerCase(), permissionList, true);
		}
		event.getChannel().sendMessage(embedBuilder.build());
		event.getMessage().delete();
		return true;
	}
}