package de.ng.wuffy.command;

import java.util.Arrays;
import java.util.HashMap;

import de.ng.wuffy.command.commands.admin.PermissionCommand;
import de.ng.wuffy.command.commands.admin.PrefixCommand;
import de.ng.wuffy.command.commands.other.HelpCommand;

public class CommandManager {
	
	public static final CommandManager COMMAND_MANAGER = new CommandManager();
	
	private final HashMap<String, BaseCommand> commands;
	
	public CommandManager() {
		commands = new HashMap<>();
	}
	
	public void registerCommands() {
		addCommand(new PrefixCommand(this));
		addCommand(new PermissionCommand(this));
		addCommand(new HelpCommand(this));
	}
	
	public void addCommand(BaseCommand command) {
		Arrays.asList(command.aliases).forEach(use -> commands.put(use.toLowerCase(), command));
	}
	
	public BaseCommand getCommand(String command) {
		return commands.get(command.toLowerCase());
	}
	
	public HashMap<String, BaseCommand> getCommands() {
		return commands;
	}
}