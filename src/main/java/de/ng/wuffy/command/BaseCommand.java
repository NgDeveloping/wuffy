package de.ng.wuffy.command;

import de.ng.wuffy.lang.TranslationKeys;
import de.ng.wuffy.permission.PermissionKeys;
import de.ng.wuffy.settings.GuildSettings;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

public abstract class BaseCommand {
	
	public abstract boolean executeCommand(MessageReceivedEvent event, GuildSettings guildSettings, String[] args) throws Exception;
	
	protected CommandManager commandManager;
	
	protected final CommandCategory category;
	
	protected final TranslationKeys name;
	protected final TranslationKeys usage;
	protected final TranslationKeys description;
	
	protected final PermissionKeys permission;
	
	protected final String[] aliases;
	
	public BaseCommand(CommandManager commandManager, CommandCategory category, TranslationKeys name, TranslationKeys usage, TranslationKeys description, PermissionKeys permission, String... aliases) {
		this.commandManager = commandManager;
		this.category = category;
		this.name = name;
		this.usage = usage;
		this.description = description;
		this.permission = permission;
		this.aliases = aliases;
	}
	
	public CommandManager getCommandManager() {
		return commandManager;
	}
	
	public CommandCategory getCategory() {
		return category;
	}
	
	public TranslationKeys getDescription() {
		return description;
	}
	
	public TranslationKeys getName() {
		return name;
	}
	
	public TranslationKeys getUsage() {
		return usage;
	}
	
	public PermissionKeys getPermission() {
		return permission;
	}
	
	public String[] getAliases() {
		return aliases;
	}
}