package de.ng.wuffy.command;

public enum CommandCategory {
	
	ADMIN,
	FUN,
	NSFW,
	MUSIC,
	OTHER
	
}