package de.ng.wuffy.listener;

import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.ReadyEvent;

public class ReadyListener {

	@EventSubscriber
	public void onReadEvent(ReadyEvent event) {
		System.out.println("Thanks, that you bring me back to life.\nI'm ready.");
	}
}