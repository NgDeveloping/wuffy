package de.ng.wuffy.listener;

import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.guild.GuildCreateEvent;

public class GuildListener {
	
	@EventSubscriber
	public void onGuildCreate(GuildCreateEvent event) {
		System.out.println("Joined new Guild: " + event.getGuild().getName());
	}
}