package de.ng.wuffy.listener;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ArrayUtils;

import de.ng.wuffy.Wuffy;
import de.ng.wuffy.command.BaseCommand;
import de.ng.wuffy.command.CommandManager;
import de.ng.wuffy.lang.I18n;
import de.ng.wuffy.lang.TranslationKeys;
import de.ng.wuffy.settings.GuildSettings;
import de.ng.wuffy.settings.GuildSettingsManager;
import de.ng.wuffy.settings.addons.GuildPermission;
import de.ng.wuffy.settings.addons.GuildStorage;
import de.ng.wuffy.util.MessageUtil;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.util.MissingPermissionsException;

public class MessageListener {
	
	@EventSubscriber
	public void onMessageReceived(MessageReceivedEvent event) {
		if(event.getMessage().isDeleted() || event.getAuthor().isBot())
			return;
		
		boolean botAuthor = Arrays.asList(Wuffy.BOT_AUTHORS).contains(event.getAuthor().getLongID());
		GuildSettings guildSettings = GuildSettingsManager.GUILD_SETTINGS_MANAGER.getGuildSettings(event.getChannel().isPrivate() ? GuildSettingsManager.PRIVATE_GUILD_SETTINGS_LONG_ID : event.getGuild().getLongID());
		GuildStorage guildStorage = guildSettings.getGuildStorage();
		String message = event.getMessage().getContent();
		
		if((guildStorage.checkIsPrefix(message) || ((botAuthor || guildStorage.isTrigger()) && guildStorage.checkIsBotTrigger(message)))) {
			message = guildStorage.removeBotPrefixesAndTriggerFromMessage(message, botAuthor);
			
			String command = message.split(" ")[0];
			String[] arguments = ArrayUtils.remove(message.substring(command.length()).split(" "), 0);
			
			Wuffy.EXECUTOR_SERVICE.execute(new Runnable() {
				
				@Override
				public void run() {
					try {
						GuildPermission guildPermission = guildSettings.getGuildPermission();
						BaseCommand baseCommand = CommandManager.COMMAND_MANAGER.getCommand(command);
						
						if(baseCommand != null) {
							boolean privateChannel = event.getChannel().isPrivate();
							
							if(botAuthor || privateChannel || guildPermission.hasChannelPermission(event.getChannel().getLongID(), baseCommand.getPermission())) {
								if(botAuthor || privateChannel || guildPermission.hasGroupPermission(event.getAuthor().getRolesForGuild(event.getGuild()), baseCommand.getPermission())) {
									if(botAuthor || !privateChannel || guildPermission.hasUserPermission(event.getAuthor().getLongID(), baseCommand.getPermission())) {
										//Has permission
										if(!baseCommand.executeCommand(event, guildSettings, arguments))
											event.getChannel().sendMessage("", MessageUtil.createHelpList(baseCommand, guildStorage.getLocale()).build());
										return;
									}
									//Has no user permission in private chat for this command
									event.getChannel().sendMessage(I18n.format(TranslationKeys.MESSAGE_ERROR_NO_USER_PERMISSION_PRIVATE_CHAT, guildStorage.getLocale()));
									return;
								}
								//Has no group permission for this command
								event.getChannel().sendMessage(I18n.format(TranslationKeys.MESSAGE_ERROR_NO_GROUP_PERMISSION, guildStorage.getLocale()));
								return;
							}
							//Has no channel permission for this command
							event.getChannel().sendMessage(I18n.format(TranslationKeys.MESSAGE_ERROR_NO_CHANNEL_PERMISSION, guildStorage.getLocale()));
							return;
						}
						//Command not found
						return;
					} catch(MissingPermissionsException ex) {
						event.getChannel().sendMessage(I18n.format(TranslationKeys.MESSAGE_ERROR_MISSNING_PERMISSION, guildStorage.getLocale(), "%PERMISSION%", String.join(" ", ex.getMissingPermissions().stream().map(perm -> perm.name()).collect(Collectors.toList()))));
					} catch(Exception ex) {
						ex.printStackTrace();
						System.out.println("Error by executing command \"" + command + "\" with args \"" + String.join(" ", arguments) + " \"\n"
										+ "Private chat: " + event.getChannel().isPrivate() + "\n"
										+ "Executor: \"" + event.getAuthor().getLongID() + "\"\n"
										+ "    Name: (\"" + event.getAuthor().getName() + "\")\n");
						if(!event.getChannel().isPrivate())
							System.out.println("Guild: \"" + event.getGuild().getLongID() + "\" (\"" + event.getGuild().getName() + "\")\n"
									+ "    Guild Name: (\"" + event.getAuthor().getNicknameForGuild(event.getGuild()) + "\")");
					}
				}
			});
			return;
		}
		//Message started not with a usage or trigger is disabled
	}
}