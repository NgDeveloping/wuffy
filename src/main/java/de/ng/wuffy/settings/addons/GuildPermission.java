package de.ng.wuffy.settings.addons;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import de.ng.wuffy.Wuffy;
import de.ng.wuffy.permission.PermissionKeys;
import de.ng.wuffy.settings.GuildSettings;
import de.ng.wuffy.settings.GuildSettingsAddon;
import de.ng.wuffy.settings.GuildSettingsManager;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.IUser;

public class GuildPermission extends GuildSettingsAddon {
	
	private static List<PermissionKeys> ALL_PERMISSION_KEYS = Arrays.asList(PermissionKeys.values());
	
	private ConcurrentHashMap<Long, List<PermissionKeys>> groupPermissions;
	private ConcurrentHashMap<Long, List<PermissionKeys>> userPermissions;
	private ConcurrentHashMap<Long, List<PermissionKeys>> channelPermissions;
	
	public GuildPermission(GuildSettings guildSettings) {
		super(guildSettings);
		
		this.guildSettings = guildSettings;
		
		groupPermissions = new ConcurrentHashMap<>();
		userPermissions = new ConcurrentHashMap<>();
		channelPermissions = new ConcurrentHashMap<>();
	}
	
	public GuildPermission(GuildSettings guildSettings, Properties properties) {
		super(guildSettings);
		
		this.guildSettings = guildSettings;
		
		groupPermissions = new ConcurrentHashMap<>();
		userPermissions = new ConcurrentHashMap<>();
		channelPermissions = new ConcurrentHashMap<>();
		
		load(properties);
	}
	
	public Properties load(Properties properties) {
		properties.forEach((key, value) -> {
			String[] args = ((String) key).split(GuildSettingsManager.SPLITTER);
			if(args.length == 2)
				if(args[0].startsWith("G"))
					setGroupPermission(PermissionKeys.searchByTranslationKeyName((String) value), Long.valueOf(args[1]), true);
				else if(args[0].startsWith("U"))
					setUserPermission(PermissionKeys.searchByTranslationKeyName((String) value), Long.valueOf(args[1]), true);
				else if(args[0].startsWith("C"))
					setChannelPermission(PermissionKeys.searchByTranslationKeyName((String) value), Long.valueOf(args[1]), true);
				else
					System.out.println("Failed to load key -> \"" + (String) key + "\" with value \"" + value + "\"");
		});
		return properties;
	}
	
	public Properties save(Properties properties) {
		groupPermissions.forEach((key, value) -> value.forEach(permission -> properties.setProperty("G" + GuildSettingsManager.SPLITTER + key + GuildSettingsManager.SPLITTER + ALL_PERMISSION_KEYS.indexOf(permission), permission.getPermission())));
		userPermissions.forEach((key, value) -> value.forEach(permission -> properties.setProperty("U" + GuildSettingsManager.SPLITTER + key + GuildSettingsManager.SPLITTER + ALL_PERMISSION_KEYS.indexOf(permission), permission.getPermission())));
		channelPermissions.forEach((key, value) -> value.forEach(permission -> properties.setProperty("C" + GuildSettingsManager.SPLITTER + key + GuildSettingsManager.SPLITTER + ALL_PERMISSION_KEYS.indexOf(permission), permission.getPermission())));
		return properties;
	}
	
	@Override
	public Properties create(Properties properties) {
		Wuffy.client.getGuildByID(guildSettings.getGuildId()).getChannels().forEach(channel -> channelPermissions.put(channel.getLongID(), ALL_PERMISSION_KEYS));
		return properties;
	}
	
	public boolean hasPermission(GroupType type, Long value, PermissionKeys permission) {
		switch (type) {
		case USER:
			return hasUserPermission(value, permission);

		case GROUP:
			return hasGroupPermission(value, permission);
			
		case CHANNEL:
			return hasChannelPermission(value, permission);
			
		default:
			System.out.println("Failed to find type \"" + type.name() + "\"");
			return false;
		}
	}
	
	public boolean hasGroupPermission(Long group, PermissionKeys permission) {
		return groupPermissions.containsKey(group) && groupPermissions.get(group).contains(permission);
	}
	
	public boolean hasUserPermission(Long user, PermissionKeys permission) {
		return userPermissions.containsKey(user) && userPermissions.get(user).contains(permission);
	}
	
	public boolean hasChannelPermission(Long channel, PermissionKeys permission) {
		return channelPermissions.containsKey(channel) && channelPermissions.get(channel).contains(permission);
	}
	
	public boolean hasGroupPermission(List<IRole> groups, PermissionKeys permission) {
		return groups.stream().anyMatch(group -> hasGroupPermission(group.getLongID(), permission));
	}
	
	public boolean hasUserPermission(List<IUser> users, PermissionKeys permission) {
		return users.stream().anyMatch(user -> hasUserPermission(user.getLongID(), permission));
	}
	
	public boolean hasChannelPermission(List<IChannel> channels, PermissionKeys permission) {
		return channels.stream().anyMatch(channel -> hasChannelPermission(channel.getLongID(), permission));
	}
	
	public boolean setPermission(GroupType type, Long value, PermissionKeys permission, boolean enabled) {
		switch (type) {
		case USER:
			return setUserPermission(permission, value, enabled);

		case GROUP:
			return setGroupPermission(permission, value, enabled);
			
		case CHANNEL:
			return setChannelPermission(permission, value, enabled);
			
		default:
			System.out.println("Failed to set type \"" + type.name() + "\"");
			return false;
		}
	}
	
	public boolean setGroupPermission(PermissionKeys permission, Long group, boolean enabled) {
		if(groupPermissions.containsKey(group)) {
			if(enabled) {
				if(!groupPermissions.get(group).contains(permission))
					return groupPermissions.get(group).add(permission);
			} else
				if(groupPermissions.get(group).contains(permission))
					return groupPermissions.get(group).remove(permission);
		} else {
			groupPermissions.put(group, new CopyOnWriteArrayList<>());
			if(enabled)
				groupPermissions.get(group).add(permission);
			return true;
		}
		return false;
	}
	
	public boolean setUserPermission(PermissionKeys permission, Long user, boolean enabled) {
		if(userPermissions.containsKey(user)) {
			if(enabled) {
				if(!userPermissions.get(user).contains(permission))
					return userPermissions.get(user).add(permission);
			} else
				if(userPermissions.get(user).contains(permission))
					return userPermissions.get(user).remove(permission);
		} else {
			userPermissions.put(user, new CopyOnWriteArrayList<>());
			if(enabled)
				userPermissions.get(user).add(permission);
			return true;
		}
		return false;
	}
	
	public boolean setChannelPermission(PermissionKeys permission, Long channel, boolean enabled) {
		if(channelPermissions.containsKey(channel)) {
			if(enabled) {
				if(!channelPermissions.get(channel).contains(permission))
					return channelPermissions.get(channel).add(permission);
			} else
				if(channelPermissions.get(channel).contains(permission))
					return channelPermissions.get(channel).remove(permission);
		} else {
			channelPermissions.put(channel, new CopyOnWriteArrayList<>());
			if(enabled)
				channelPermissions.get(channel).add(permission);
			return true;
		}
		return false;
	}
	
	public ConcurrentHashMap<Long, List<PermissionKeys>> getGroupPermissions() {
		return groupPermissions;
	}
	
	public ConcurrentHashMap<Long, List<PermissionKeys>> getUserPermissions() {
		return userPermissions;
	}
	
	public ConcurrentHashMap<Long, List<PermissionKeys>> getChannelPermissions() {
		return channelPermissions;
	}
	
	public GuildSettings getGuildSettings() {
		return guildSettings;
	}
	
	public static enum GroupType {
		GROUP, USER, CHANNEL
	}
}