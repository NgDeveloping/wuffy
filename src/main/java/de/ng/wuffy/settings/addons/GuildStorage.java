package de.ng.wuffy.settings.addons;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.CopyOnWriteArrayList;

import de.ng.wuffy.Wuffy;
import de.ng.wuffy.settings.GuildSettings;
import de.ng.wuffy.settings.GuildSettingsAddon;

public class GuildStorage extends GuildSettingsAddon {
	
	private String locale;
	private List<String> prefixes;
	private boolean trigger;
	
	public GuildStorage(GuildSettings guildSettings) {
		super(guildSettings);
		
		this.locale = "en_US";
		this.trigger = true;
		this.prefixes = new CopyOnWriteArrayList<>();
	}

	public GuildStorage(GuildSettings guildSettings, Properties properties) {
		super(guildSettings);
		
		this.locale = "en_US";
		this.trigger = true;
		this.prefixes = new CopyOnWriteArrayList<>();
		
		load(properties);
	}

	@Override
	public Properties load(Properties properties) {
		this.locale = properties.containsKey("locale") ? properties.getProperty("locale") : "en_US";
		this.trigger = properties.containsKey("trigger") ? Boolean.valueOf(properties.getProperty("trigger")) : false;
		properties.forEach((key, value) -> { if(((String) key).startsWith("prefix-")) this.prefixes.add((String) value); });
		return properties;
	}
	
	@Override
	public Properties save(Properties properties) {
		properties.setProperty("locale", locale);
		properties.setProperty("trigger", Boolean.toString(trigger));
		prefixes.forEach(use -> properties.setProperty("prefix-" + prefixes.indexOf(use), use));
		return properties;
	}
	
	@Override
	public Properties create(Properties properties) {
		properties.setProperty("locale", "en_US");
		properties.setProperty("trigger", "true");
		properties.setProperty("prefix-0", "~");
		return properties;
	}
	
	public String removeBotPrefixesAndTriggerFromMessage(String message, boolean botAuthor) {
		for(String use : prefixes)
			if(message.toLowerCase().startsWith(use.toLowerCase())) {
				message = message.substring(use.length());
				break;
			}
		
		if((botAuthor || trigger) && checkIsBotTrigger(message))
			message = message.substring(Wuffy.client.getApplicationClientID().length() + 3);
		
		while(message.startsWith(" "))
			message = message.substring(1);
		return message;
	}
	
	public boolean checkIsPrefix(String message) {
		return prefixes.stream().anyMatch(use -> message.toLowerCase().startsWith(use.toLowerCase()));
	}
	
	public boolean checkIsBotTrigger(String message) {
		String clientId = Wuffy.client.getApplicationClientID();
		return message.length() > (clientId.length() + 1) && message.substring(2, clientId.length() + 2).equals(clientId);
	}
	
	public void addPrefix(String prefix) {
		this.prefixes.add(prefix);
	}
	
	public void removePrefix(String prefix) {
		this.prefixes.remove(prefix);
	}
	
	public void setTrigger(boolean trigger) {
		this.trigger = trigger;
	}
	
	public void setPrefixes(List<String> prefixes) {
		this.prefixes = prefixes;
	}
	
	public void setLocale(String locale) {
		this.locale = locale;
	}
	
	public boolean isTrigger() {
		return trigger;
	}
	
	public List<String> getPrefixes() {
		return prefixes;
	}
	
	public String getLocale() {
		return locale;
	}
}