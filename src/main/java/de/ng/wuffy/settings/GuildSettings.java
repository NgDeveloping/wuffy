package de.ng.wuffy.settings;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;

import de.ng.wuffy.settings.addons.GuildPermission;
import de.ng.wuffy.settings.addons.GuildStorage;
import de.ng.wuffy.util.PropertiesUtil;
import de.ng.wuffy.util.SimpleEntry;

public class GuildSettings {

	private GuildPermission guildPermission;
	private GuildStorage guildStorage;
	
	private File folder;
	private Long guildId;

	private List<Entry<File, Properties>> storageCache;
	private List<Entry<File, Properties>> oldStorageCache;

	public GuildSettings(File folder) {
		this.folder = folder;
		this.guildId = Long.valueOf(folder.getName());
		this.storageCache = new ArrayList<>();
		this.oldStorageCache = new ArrayList<>();

		if(!folder.exists())
			folder.mkdirs();

		this.guildPermission = new GuildPermission(this);
		this.guildStorage = new GuildStorage(this);

		load();
		createOldStorageCache();
	}

	public void load() {
		guildPermission.load(PropertiesUtil.load(new File(folder, "permission.properties")));
		guildStorage.load(PropertiesUtil.load(new File(folder, "storage.properties")));
		createStorageCache();
	}

	public void save() {
		createStorageCache();
		storageCache.forEach(cache -> PropertiesUtil.save(cache.getKey(), cache.getValue()));
		createOldStorageCache();
	}
	
	public void setup() {
		storageCache.clear();
		storageCache.add(new SimpleEntry<File, Properties>(new File(folder, "permission.properties"), guildPermission.setup(new Properties())));
		storageCache.add(new SimpleEntry<File, Properties>(new File(folder, "storage.properties"), guildStorage.setup(new Properties())));
	}
	
	public void createStorageCache() {
		storageCache.clear();
		storageCache.add(new SimpleEntry<File, Properties>(new File(folder, "permission.properties"), guildPermission.save(new Properties())));
		storageCache.add(new SimpleEntry<File, Properties>(new File(folder, "storage.properties"), guildStorage.save(new Properties())));
	}
	
	public void createOldStorageCache() {
		oldStorageCache.clear();
		oldStorageCache.addAll(storageCache);
	}
	
	public boolean hasChanges() {
		createStorageCache();
		if(oldStorageCache.size() != storageCache.size()) {
			createOldStorageCache();
			return false;
		}
		
		for(int i = 0; i < oldStorageCache.size(); i++) {
			System.out.println(i + " - " + (storageCache.size() < i));
			System.out.println(i + " - " + (!storageCache.get(i).getKey().equals(oldStorageCache.get(i).getKey())));
			System.out.println(i + " - " + (!storageCache.get(i).getValue().equals(oldStorageCache.get(i).getValue())));
			if(storageCache.size() < i ||
					!storageCache.get(i).getKey().equals(oldStorageCache.get(i).getKey()) ||
					!storageCache.get(i).getValue().equals(oldStorageCache.get(i).getValue()))
				return true;
		}
		return false;
	}
	
	public GuildPermission getGuildPermission() {
		return guildPermission;
	}
	
	public GuildStorage getGuildStorage() {
		return guildStorage;
	}

	public Long getGuildId() {
		return guildId;
	}

	public File getFolder() {
		return folder;
	}
}