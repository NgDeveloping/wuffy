package de.ng.wuffy.settings;

import java.util.Properties;

public abstract class GuildSettingsAddon {
	
	public abstract Properties load(Properties properties);
	public abstract Properties save(Properties properties);
	protected abstract Properties create(Properties properties);
	
	protected GuildSettings guildSettings;
	
	public GuildSettingsAddon(GuildSettings guildSettings) {
		this.guildSettings = guildSettings;
	}
	
	public Properties setup(Properties properties) {
		create(properties);
		load(properties);
		return properties;
	}
	
	public GuildSettings getGuildSettings() {
		return guildSettings;
	}
}