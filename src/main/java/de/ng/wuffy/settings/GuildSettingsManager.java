package de.ng.wuffy.settings;

import java.io.File;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

public class GuildSettingsManager {

	public static final GuildSettingsManager GUILD_SETTINGS_MANAGER = new GuildSettingsManager();
	
	public static final Long PRIVATE_GUILD_SETTINGS_LONG_ID = -1L;
	public static final File DEFAULT_DIRECTION = new File("./wuffy/guilds/");
	public static final String SPLITTER = "~";
	
	private ConcurrentHashMap<Long, GuildSettings> GUILDS;
	
	private Thread thread;
	
	public GuildSettingsManager() {
		GUILDS = new ConcurrentHashMap<>();
		startSaveThread();
	}
	
	private void startSaveThread() {
		thread = new Thread(() -> {
			while(true)
				try {
					Thread.sleep(60000);
					System.out.println("SAVE");
					int count = 0;
					for(Entry<Long, GuildSettings> entry : GUILDS.entrySet()) {
						GuildSettings guildSettings = entry.getValue();
						try {
							if(guildSettings.hasChanges()) {
								guildSettings.save();
								count++;
							}
						} catch(Exception ex) {
							ex.printStackTrace();
							if(guildSettings != null)
								System.out.println("Failed to save Guild \"" + guildSettings.getGuildId() + "\" - \"" + guildSettings.getFolder().getAbsolutePath() + "\"");
							else {
								System.out.println("Failed to save Guild. Guild was null removing guild from hashmap. guildId \"" + entry.getKey() + "\n");
								GUILDS.remove(entry.getKey());
							}
						}
					}
					if(count != 0)
						System.out.println("Stored " + count + " guild settings.");
				} catch (InterruptedException ex) {
					ex.printStackTrace();
				}
		});
		thread.setDaemon(true);
		thread.start();
	}

	public GuildSettings getGuildSettings(Long guildId) {
		if(!GUILDS.containsKey(guildId)) {
			GuildSettings guildSettings = new GuildSettings(new File(DEFAULT_DIRECTION, guildId.toString()));
			GUILDS.put(guildId, guildSettings);
			guildSettings.setup();
			return guildSettings;
		}
		return GUILDS.get(guildId);
	}

	public void loadAllGuilds() {
		for(File file : DEFAULT_DIRECTION.listFiles())
			GUILDS.put(Long.valueOf(file.getName()), new GuildSettings(file));
		System.out.println("Loaded " + GUILDS.size() + " guild settings.");
	}

	public void saveAllGuilds() {
		GUILDS.values().forEach(guild -> guild.save());
		System.out.println("Stored " + GUILDS.size() + " guild settings.");
	}
}