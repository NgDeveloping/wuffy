package de.ng.wuffy.permission;

import de.ng.wuffy.command.CommandCategory;

public enum PermissionKeys {
	
	/*
	 * Commands admin
	 */
	COMMAND_ADMIN_PREFIX("admin.prefix", CommandCategory.ADMIN),
	COMMAND_ADMIN_PERMISSION("admin.permission", CommandCategory.ADMIN),
	
	/*
	 * Commands other
	 */
	COMMAND_OTHER_HELP("other.help", CommandCategory.OTHER);
	
	private String permission;
	private CommandCategory category;
	
	private PermissionKeys(String permission, CommandCategory category) {
		this.permission = permission;
		this.category = category;
	}
	
	public String getPermission() {
		return permission;
	}
	
	public CommandCategory getCategory() {
		return category;
	}
	
	public static PermissionKeys searchByTranslationKeyName(String translationKeyName) {
		for(PermissionKeys permission : values())
			if(permission.getPermission().equalsIgnoreCase(translationKeyName))
				return permission;
		return null;
	}
}