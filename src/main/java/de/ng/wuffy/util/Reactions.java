package de.ng.wuffy.util;

import sx.blah.discord.handle.impl.obj.ReactionEmoji;

public enum Reactions {
	
	NO_ENTRY("⛔"),
	HEART("❤"),
	TADA("🎉"),
	WHITE_CHECK_MARK("✅"),
	THUMBSUP("👍"),
	INTERROBANG("⁉"),
	GREY_QUESTION("❔"),
	WARNING("⚠"),
	ANGER("💢"),
	HEARTPUlSE("💗"),
	HEART_DECORATION("💟"),
	HEART_EXCLAMATION("❣"),
	SPARKLING_HEART("💖"),
	BROKEN_HEART("💔"),
	HEARTS("♥");
	
	private String emoji;
	
	private Reactions(String emoji) {
		this.emoji = emoji;
	}
	
	public String getAsUnicode() {
		return this.emoji;
	}
	
	public ReactionEmoji getAsReactionEmoji() {
		return ReactionEmoji.of(this.emoji);
	}
}