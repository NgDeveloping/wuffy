package de.ng.wuffy.util;

import java.util.Random;

import de.ng.wuffy.Wuffy;
import de.ng.wuffy.command.BaseCommand;
import de.ng.wuffy.lang.I18n;
import de.ng.wuffy.lang.TranslationKeys;
import de.ng.wuffy.permission.PermissionKeys;
import sx.blah.discord.util.EmbedBuilder;

public class MessageUtil {
	
	public static final Random RANDOM = new Random();
	
	public static EmbedBuilder createEmbedBuilder(String description) {
		return createEmbedBuilder()
				.appendDescription(description);
	}
	
	public static EmbedBuilder createEmbedBuilder()	{
		return new EmbedBuilder()
				.withAuthorIcon(Wuffy.EMBED_AUTHOR_ICON)
				.withAuthorName(Wuffy.EMBED_AUTHOR_NAME)
				.withAuthorUrl(Wuffy.EMBED_AUTHOR_URL)
				.withColor(RANDOM.nextInt(100) + 155, RANDOM.nextInt(100) + 155, RANDOM.nextInt(100) + 155);
	}
	
	public static EmbedBuilder createHelpList(TranslationKeys description, TranslationKeys usage, PermissionKeys permission, String locale) {
		return createEmbedBuilder(I18n.format(description, locale))
				.appendField(I18n.format(TranslationKeys.MESSAGE_INFO_USAGE, locale), I18n.format(usage, locale), true)
				.appendField(I18n.format(TranslationKeys.MESSAGE_INFO_PERMISSION, locale), permission.getPermission(), true);
	}
	
	public static EmbedBuilder createHelpList(BaseCommand command, String locale) {
		return createHelpList(command.getDescription(), command.getUsage(), command.getPermission(), locale);
	}
}