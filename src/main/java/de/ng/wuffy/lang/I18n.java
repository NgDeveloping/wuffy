package de.ng.wuffy.lang;

public class I18n {
	
	public static String format(TranslationKeys translateKey, String locale, String... parameters) {
		Language lang = LanguageManager.LANGUAGE_MANAGER.getLanguageByLocale(locale);
		String message = lang != null ? lang.getProperty(translateKey.getKey()) : translateKey.getKey();
		
		String replace = "";
		for(String parameter : parameters)
			if(replace.isEmpty())
				replace = parameter;
			else {
				message = message.replaceAll(replace, parameter);
				replace  = "";
			}
		
		return message;
	}
}