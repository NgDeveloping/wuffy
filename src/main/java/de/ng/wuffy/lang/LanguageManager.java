package de.ng.wuffy.lang;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class LanguageManager {
	
	public static final LanguageManager LANGUAGE_MANAGER = new LanguageManager(new File("./wuffy/lang"));
	public static final String DEFAULT_LANGUAGE = "en_US";
	
	private File folder;
	private List<Language> languages = new ArrayList<>();
	
	public LanguageManager(File folder) {
		this.folder = folder;
	}
	
	public void load(String defaultLocale) {
		if(!folder.exists()) {
			try {
				System.out.println("Coudn't find lang folder. generating...");
//				FileUtil.copyLocalResourceFolderToFolder("cloud/lang", new File("./wuffy/lang"));
			} catch(Exception ex) {
				System.out.println("Failed to create lang folder.");
				ex.printStackTrace();
			}
		}
		
		for(File file : folder.listFiles())
			if(!file.isDirectory()) {
				Language language = new Language(new Locale(file.getName().replace(".properties", "")), file);
				if(language.load())
					languages.add(language);
			}
	}
	
	public Language getLanguageByLocale(String locale) {
		for(Language language : languages)
			if(language.getLocale().getLanguage().equalsIgnoreCase(locale) || language.getLocale().toLanguageTag().equalsIgnoreCase(locale))
				return language;
		return null;
	}
}