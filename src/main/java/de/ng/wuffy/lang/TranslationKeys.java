package de.ng.wuffy.lang;

public enum TranslationKeys {
	
	/*
	 * Command: prefix
	 */
	COMMAND_PREFIX("command.prefix"),
	COMMAND_PREFIX_USAGE("command.prefix.usage"),
	COMMAND_PREFIX_DESCRIPTION("command.prefix.description"),
	MESSAGE_PREFIX_HELP_ADD_PREFIX("message.prefix.embed.help.add.prefix"),
	MESSAGE_PREFIX_HELP_REMOVE_PREFIX("message.prefix.embed.help.remove.prefix"),
	MESSAGE_PREFIX_ADDED_PREFIX("message.prefix.embed.added.prefix"),
	MESSAGE_PREFIX_REMOVED_PREFIX("message.prefix.embed.removed.prefix"),
	MESSAGE_PREFIX_PREFIX_ALREADY_EXIST("message.prefix.embed.prefix.already.exist"),
	MESSAGE_PREFIX_PREFIX_NOT_FOUND("message.prefix.embed.prefix.not.found"),
	MESSAGE_PREFIX_YOU_CAN_NOT_REMOVE_THE_LAST_PREFIX("message.prefix.embed.you.can.not.remove.the.last.prefix"),
	MESSAGE_PREFIX_YOU_HAVE_THE_MAXIMUM_AT_PREFIXES("message.prefix.embed.you.have.the.maximum.at.prefixes"),
	MESSAGE_PREFIX_LIST_OF_PREFIXES("message.prefix.embed.list.of.prefixes"),
	
	/*
	 * Command: permission
	 */
	COMMAND_PERMISSION("command.permission"),
	COMMAND_PERMISSION_USAGE("command.permission.usage"),
	COMMAND_PERMISSION_DESCRIPTION("command.permission.description"),
	MESSAGE_PERMISSION_EMPTY("message.permission.empty"),
	
	/*
	 * Command: help
	 */
	COMMAND_HELP("command.help"),
	COMMAND_HELP_USAGE("command.help.usage"),
	COMMAND_HELP_DESCRIPTION("command.help.description"),
	MESSAGE_HELP_EMPTY("message.help.empty"),
	MESSAGE_HELP_COMMAND_NOT_FOUND("message.help.command.not.found"),
	MESSAGE_HELP_COMMAND_INFO("message.help.command.info"),
	MESSAGE_HELP_COMMAND_ALIASES("message.help.command.aliases"),
	
	/*
	 * Info messages
	 */
	
	MESSAGE_INFO_USAGE("message.info.usage"),
	MESSAGE_INFO_PERMISSION("message.info.permission"),
	
	/*
	 * Error messages
	 */
	MESSAGE_ERROR_MISSNING_PERMISSION("message.error.missing.permissions"),
	MESSAGE_ERROR_NO_USER_PERMISSION_PRIVATE_CHAT("message.error.no.user.permission.privat.chat"),
	MESSAGE_ERROR_NO_GROUP_PERMISSION("message.error.no.group.permission"),
	MESSAGE_ERROR_NO_CHANNEL_PERMISSION("message.error.no.channel.permission");
	
	private String key;
	
	private TranslationKeys(String key) {
		this.key = key;
	}
	
	public String getKey() {
		return key;
	}
}